var App = window.App = Ember.Application.create({
//    LOG_TRANSITIONS_INTERNAL: true
});

App.ApplicationRoute = Ember.Route.extend({
    model: function () {
        return ['red', 'yellow', 'blue'];
    }
});

/* Order and include as you please. */
require('scripts/store');
//require('scripts/controllers/*');
//require('scripts/models/*');
//require('scripts/routes/*');
//require('scripts/views/*');
//require('scripts/router');
require('scripts/core/*');
require('scripts/objects/*');
